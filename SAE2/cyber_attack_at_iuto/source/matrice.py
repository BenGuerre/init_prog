"""
             Projet CyberAttack@IUT'O
        SAÉ1.01 département Informatique IUT d'Orléans 2021-2022

    Module matrice.py
    Ce module de gestion des matrices
"""


def creer_matrice(nb_lig, nb_col, val_defaut=None):
    """créer une matrice contenant nb_lig lignes et nb_col colonnes avec
       pour valeur par défaut val_defaut

    Args:
        nb_lig (int): un entier strictement positif
        nb_col (int): un entier strictement positif
        val_defaut (Any, optional): La valeur par défaut des éléments de la matrice.
                                    Defaults to None.
    Returns:
        dict: la matrice
    """
    return (nb_lig, nb_col, [val_defaut] * (nb_col * nb_lig)) 


def get_nb_lignes(matrice):
    """retourne le nombre de lignes de la matrice

    Args:
        matrice (dict): une matrice

    Returns:
        int: le nombre de lignes de la matrice
    """
    return matrice[0]


def get_nb_colonnes(matrice):
    """retourne le nombre de colonnes de la matrice

    Args:
        matrice (dict): une matrice

    Returns:
        int: le nombre de colonnes de la matrice
    """
    return matrice[1]


def get_val(matrice, lig, col):
    """retourne la valeur en lig, col de la matrice

    Args:
        matrice (dict): une matrice
        lig (int): numéro de la ligne (en commençant par 0)
        col (int): numéro de la colonne (en commençant par 0)

    Returns:
        Any: la valeur en lig, col de la matrice
    """
    return matrice[2][(lig * matrice[1]) + col]


def set_val(matrice, lig, col, val):
    """stocke la valeur val en lig, col de la matrice

    Args:
        matrice (dict): une matrice
        lig (int): numéro de la ligne (en commençant par 0)
        col (int): numéro de la colonne (en commençant par 0)
        val (Any): la valeur à stocker
    """
    matrice[2][(lig * matrice[1]) + col] = val


def max_matrice(matrice, interdits=None):
    """retourne la liste des coordonnées des case contenant la valeur la plus grande de la matrice
        Ces case ne doivent pas être parmis les interdits

    Args:
        matrice (dict): une matrice
        interdits (set): un ensemble de tuples (ligne,colonne) de case interdites. Defaults to None

    Returns:
        list: les coordonnées de case de valeur maximale dans la matrice (hors cases interdites)
    """
    max = None
    co_max = []
    for ligne in range(get_nb_lignes(matrice)):
        for colonne in range(get_nb_colonnes(matrice)):
            val = get_val(matrice, ligne, colonne)
            if val is not None:
                if (interdits is None or (ligne, colonne) not in interdits) and (max is None or val >= max):
                    if val != max:
                        max = val
                        co_max = [(ligne, colonne)]
                    else:
                        co_max.append((ligne, colonne))
    return co_max




DICO_DIR = {(-1, -1): 'HD', (-1, 0): 'HH', (-1, 1): 'HD', (0, -1): 'GG',
            (0, 1): 'DD', (1, -1): 'BG', (1, 0): 'BB', (1, 1): 'BD', (0, 0): 'BB'}


def direction_max_voisin(matrice, ligne, colonne):
    """retourne la liste des directions qui permette d'aller vers la case voisine de (ligne,colonne)
       la plus grande avec en plus la direction qui permet de se rapprocher du milieu de la matrice
       si ligne,colonne n'est pas le milieu de la matrice

    Args:
        matrice (dict): une matrice
        ligne (int): le numéro de la ligne de la case considérée
        colonne (int): le numéro de la colonne de la case considérée

    Returns:
        str: deux lettres indiquant la direction DD -> droite , HD -> Haut Droite,
                                                 HH -> Haut, HG -> Haut gauche,
                                                 GG -> Gauche, BG -> Bas Gauche, BB -> Bas
    """
    largeur = get_nb_colonnes(matrice)
    hauteur = get_nb_lignes(matrice)
    voisin = []
    if ligne - 1 in range(0, hauteur):
        voisin.append(("HH", ligne - 1, colonne))
    if colonne - 1 in range(0, largeur):
        voisin.append(("GG", ligne, colonne - 1))
    if ligne - 1 in range(0, hauteur) and colonne - 1 in range(0, largeur):
        voisin.append(("HG", ligne - 1, colonne - 1))
    if ligne + 1 in range(0, hauteur):
        voisin.append(("BB", ligne + 1, colonne))
    if colonne + 1 in range(0, largeur):
        voisin.append(("DD", ligne, colonne + 1))
    if ligne + 1 in range(0, hauteur) and colonne + 1 in range(0, largeur):
        voisin.append(("BD", ligne + 1, colonne + 1))
    if ligne + 1 in range(0, hauteur) and colonne - 1 in range(0, largeur):
        voisin.append(("BG", ligne + 1, colonne - 1))
    if ligne - 1 in range(0, hauteur) and colonne + 1 in range(0, largeur):
        voisin.append(("HD", ligne - 1, colonne + 1))
    max_val = None
    liste_direction = []
    for (direction, x, y) in voisin:
        val = get_val(matrice, x, y)
        if max_val is None or val >= max_val:
            if max_val == val:
                liste_direction.append(direction)
            else:
                max_val = val
                liste_direction = [direction]
    return liste_direction

        

