"""
             Projet CyberAttack@IUT'O
        SAÉ1.01 département Informatique IUT d'Orléans 2021-2022

    Module joueur.py
Module de gestion des joueurs
"""


def creer_joueur(id_joueur, nom_joueur, nb_points=0):
    """créer un nouveau joueur

    Args:
        id_joueur (int): l'identifiant du joueur (un entier de 1 à 4)
        nom_joueur (str): le nom du joueur
        nb_points (int, optional): le nombre de points du joueur. Defaults to 0.

    Returns:
        dict: le joueur
    """
    if isinstance(id_joueur, int) and 0 < id_joueur <= 4 and isinstance(nom_joueur, str):
        return{"id": id_joueur, "nom": nom_joueur, "points": nb_points}
    return None

def get_id(joueur):
    """retourne l'identifiant du joueur

    Args:
        joueur (dict): un joueur

    Returns:
        int: l'identifiant du joueur
    """
    if not joueur is None:
        return joueur["id"]
    return None


def get_nom(joueur):
    """retourne le nom du joueur

    Args:
        joueur (dict): un joueur

    Returns:
        str: nom du joueur
    """
    if not joueur is None:
        return joueur["nom"]
    return None


def get_points(joueur):
    """retourne le nombre de points du joueur

    Args:
        joueur (dict): un joueur

    Returns:
        int: le nombre de points du joueur
    """
    if not joueur is None:
        return joueur["points"]
    return None


def ajouter_points(joueur, points):
    """ajoute des points au joueur

    Args:
        joueur (dict): un joueur
        points (int): le nombre de points à ajouter

    Returns:
        int: le nombre de points du joueur
    """
    if not joueur is None and isinstance(points, int):
        joueur["points"] += points
        return get_points(joueur)
    return None

#plan:  3
#    4     2
#       1
def id_joueur_droite(joueur):
    """retourne l'identifiant du joueur à droite d'un joueur

    Args:
        joueur (dict): un joueur

    Returns:
        int: l'identifiant du joueur de droite
    """
    if joueur is not None:
        left_player = get_id(joueur) + 1
        if left_player > 4:
            left_player -= 4
        return left_player
    return None

def id_joueur_gauche(joueur):
    """retourne l'identifiant du joueur à gauche d'un joueur

    Args:
        joueur (dict): un joueur

    Returns:
        int: l'identifiant du joueur de gauche
    """
    if joueur is not None:
        right_player = get_id(joueur) - 1
        if right_player < 1:
            right_player += 4
        return right_player
    return None


def id_joueur_haut(joueur):
    """retourne l'identifiant du joueur au dessus d'un joueur

    Args:
        joueur (dict): un joueur

    Returns:
        int: l'identifiant du joueur du haut
    """
    if joueur is not None:
        top_player = get_id(joueur)
        if top_player in {1, 2}:
            top_player += 2
        else:
            top_player -= 2
        return top_player
    return None
