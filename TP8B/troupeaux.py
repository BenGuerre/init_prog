# TP8 B - Manipuler des listes, ensembles et dictionnaires

#Ex1
#1.1) le troupeau de jean contient 12, vaches 17 cochons,3veaux
# le troupeau vide ne contient rien 

#1.2
def total_animaux(troupeau):
    """ Calcule le nombre total d'animaux dans un troupeau

    Args:
        troupeau (dict): un dictionnaire modélisant un troupeau {nom_animaux: nombre}

    Returns:
        int: le nombre total d'animaux dans le troupeau
    """
    somme = 0
    for nb_animaux in troupeau.values():
        somme += nb_animaux
    return somme

def tous_les_animaux(troupeau):
    """ Détermine l'ensemble des animaux dans un troupeau

    Args:
        troupeau (dict): un dictionnaire modélisant un troupeau {nom_animaux: nombre}

    Returns:
        set: l'ensemble des animaux du troupeau
    """
    liste_animaux = set()
    for nom_animaux in troupeau.keys():
        liste_animaux.add(nom_animaux)
    return liste_animaux


def specialise(troupeau):
    """ Vérifie si le troupeau contient 30 individus ou plus d'un même type d'animal 

    Args:
        troupeau (dict): un dictionnaire modélisant un troupeau {nom_animaux: nombre}

    Returns:
        bool: True si le troupeau contient 30 (ou plus) individus d'un même type d'animal,
        False sinon 
    """
    for nb_animaux in troupeau.values():
        if nb_animaux > 30:
            return True
    return False
    


def le_plus_represente(troupeau):
    """ Recherche le nom de l'animal qui a le plus d'individus dans le troupeau
    
    Args:
        troupeau (dict): un dictionnaire modélisant un troupeau {nom_animaux: nombre}

    Returns:
        str: le nom de l'animal qui a le plus d'individus  dans le troupeau
        None si le troupeau est vide) 
    
    """
    max_rpz = 0
    nom_max_rpz = None
    for (nom_animal, nb_animal) in troupeau.items():
        if nb_animal > max_rpz:
            max_rpz = nb_animal
            nom_max_rpz = nom_animal
    return nom_max_rpz


def quantite_suffisante(troupeau):
    """ Vérifie si le troupeau contient au moins 5 individus de chaque type d'animal

    Args:
        troupeau (dict): un dictionnaire modélisant un troupeau {nom_animaux: nombre}

    Returns:
        bool: True si le troupeau contient au moins 5 individus de chaque type d'animal
        False sinon    
    """
    for nb_aniamus in troupeau.values():
        if nb_aniamus < 5:
            return False
    return True


def reunion_troupeaux(troupeau1, troupeau2):
    """ Simule la réunion de deux troupeaux

    Args:
        troupeau1 (dict): un dictionnaire modélisant un premier troupeau {nom_animaux: nombre}
        troupeau2 (dict): un dictionnaire modélisant un deuxième troupeau        

    Returns:
        dict: le dictionnaire modélisant la réunion des deux troupeaux    
    """
    nouveau_troupeau = troupeau1.copy()
    for (nom_animal, nb_animal) in troupeau2.items():
        if nom_animal in nouveau_troupeau:
            nouveau_troupeau[nom_animal] += nb_animal
        else:
            nouveau_troupeau[nom_animal] = nb_animal
    return nouveau_troupeau


#EX2