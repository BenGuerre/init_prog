"""
Permet de modéliser un le_plateau de jeu avec :
    - une matrice qui contient des nombres entiers
    - chaque nombre entier correspond à un item :
      MUR, COULOIR, PERSONNAGE, FANTOME
"""
import matrice

MUR = 1
COULOIR = 0
PERSONNAGE = 2
FANTOME = 3

NORD = 'z'
OUEST = 'q'
SUD = 'w'
EST = 's'


def init(nom_fichier="./labyrinthe1.txt"):
    """Construit le plateau de jeu de la façon suivante :
        - crée une matrice à partir d'un fichier texte qui contient des COULOIR et MUR
        - met le PERSONNAGE en haut à gauche cad à la position (0, 0)
        - place un FANTOME en bas à droite
    Args:
        nom_fichier (str, optional): chemin vers un fichier csv qui contient COULOIR et MUR.
        Defaults to "./labyrinthe1.txt".

    Returns:
        le plateau de jeu avec les MUR, COULOIR, PERSONNAGE et FANTOME
    """
    plateau = matrice.charge_matrice(nom_fichier)
    matrice.set_val(plateau, 0, 0, PERSONNAGE)
    matrice.set_val(plateau, matrice.get_nb_lignes(plateau) - 1 , matrice.get_nb_colonnes(plateau) - 1, FANTOME)
    return plateau

def est_sur_le_plateau(le_plateau, position):
    """Indique si la position est bien sur le plateau

    Args:
        le_plateau (plateau): un plateau de jeu
        position (tuple): un tuple de deux entiers de la forme (no_ligne, no_colonne) 

    Returns:
        [boolean]: True si la position est bien sur le plateau
    """
    if 0 <= position[0] <= matrice.get_nb_lignes(le_plateau) - 1  and (0 <= position[1] <= matrice.get_nb_lignes(le_plateau) - 1):
        return True
    return False


def get(le_plateau, position):
    """renvoie la valeur de la case qui se trouve à la position donnée

    Args:
        le_plateau (plateau): un plateau de jeu
        position (tuple): un tuple d'entiers de la forme (no_ligne, no_colonne)

    Returns:
        int: la valeur de la case qui se trouve à la position donnée ou
             None si la position n'est pas sur le plateau
    """
    if est_sur_le_plateau(le_plateau, position):
        return matrice.get_val(le_plateau, position[0], position[1])
    return None



def est_un_mur(le_plateau, position):
    """détermine s'il y a un mur à la poistion donnée

    Args:
        le_plateau (plateau): un plateau de jeu
        position (tuple): un tuple d'entiers de la forme (no_ligne, no_colonne)

    Returns:
        bool: True si la case à la position donnée est un MUR, False sinon
    """
    return get(le_plateau, position) == MUR
        


def contient_fantome(le_plateau, position):
    """Détermine s'il y a un fantôme à la position donnée

    Args:
        le_plateau (plateau): un plateau de jeu
        position (tuple): un tuple de deux entiers de la forme (no_ligne, no_colonne) 

    Returns:
        bool: True si la case à la position donnée est un FANTOME, False sinon
    """
    return get(le_plateau, position) == FANTOME

def est_la_sortie(le_plateau, position):
    """Détermine si la position donnée est la sortie
       cad la case en bas à droite du labyrinthe

    Args:
        le_plateau (plateau): un plateau de jeu
        position (tuple): un tuple de deux entiers de la forme (no_ligne, no_colonne) 

    Returns:
        bool: True si la case à la position donnée est la sortie, False sinon
    """
    return (matrice.get_nb_lignes(le_plateau) - 1,matrice.get_nb_colonnes(le_plateau) - 1) == position


def deplace_personnage(le_plateau, personnage, direction):
    """déplace le PERSONNAGE sur le plateau si le déplacement est valide
       Le personnage ne peut pas sortir du plateau ni traverser les murs
       Si le déplacement n'est pas valide, le personnage reste sur place

    Args:
        le_plateau (plateau): un plateau de jeu
        personnage (tuple): la position du personnage sur le plateau
        direction (str): la direction de déplacement SUD, EST, NORD, OUEST

    Returns:
        [tuple]: la nouvelle position du personnage
    """
    #calcule la nouvelle position théorique du personnage
    if direction == NORD:
        nv_personnage = (personnage[0] - 1, personnage[1])
    elif direction == SUD:
        nv_personnage = (personnage[0] + 1, personnage[1])
    elif direction == EST:
        nv_personnage = (personnage[0], personnage[1] + 1)
    elif direction == OUEST:
        nv_personnage = (personnage[0], personnage[1] - 1)
    else:
        nv_personnage = personnage
    #depalve ou non le perso
    if not est_un_mur(le_plateau, nv_personnage) and est_sur_le_plateau(le_plateau, nv_personnage):
        matrice.set_val(le_plateau, personnage[0], personnage[1], COULOIR)
        matrice.set_val(le_plateau, nv_personnage[0], nv_personnage[1], PERSONNAGE)
        return nv_personnage
    else:
        return personnage 


def voisins(le_plateau, position):
    """Renvoie l'ensemble des positions cases voisines accessibles de la position renseignées
       Une case accessible est une case qui est sur le plateau et qui n'est pas un mur
    Args:
        le_plateau (plateau): un plateau de jeu
        position (tuple): un tuple de deux entiers de la forme (no_ligne, no_colonne) 

    Returns:
        set: l'ensemble des positions des cases voisines accessibles
    """
    positions_disponible = set()
    #on test la position nord
    position_test = (position[0] - 1, position[1])
    if not est_un_mur(le_plateau, position_test) and est_sur_le_plateau(le_plateau, position_test):
        positions_disponible.add(position_test)
    
    #on test ma position sud
    position_test = (position[0] + 1, position[1])
    if not est_un_mur(le_plateau, position_test) and est_sur_le_plateau(le_plateau, position_test):
        positions_disponible.add(position_test)
    
    #on test la position est
    position_test = (position[0], position[1] + 1)
    if not est_un_mur(le_plateau, position_test) and est_sur_le_plateau(le_plateau, position_test):
        positions_disponible.add(position_test)
    
    #on test la position ouest
    position_test = (position[0], position[1] - 1)
    if not est_un_mur(le_plateau, position_test) and est_sur_le_plateau(le_plateau, position_test):
        positions_disponible.add(position_test)
    return positions_disponible

def fabrique_le_calque(le_plateau, position_depart):
    """fabrique le calque d'un labyrinthe en utilisation le principe de l'inondation :
       
    Args:
        le_plateau (plateau): un plateau de jeu
        position_depart (tuple): un tuple de deux entiers de la forme (no_ligne, no_colonne) 

    Returns:
        matrice: une matrice qui a la taille du plateau dont la case qui se trouve à la
       position_de_depart est à 0 les autres cases contiennent la longueur du
       plus court chemin pour y arriver (les murs et les cases innaccessibles sont à None)
    """
    #create calque
    nb_ligne = matrice.get_nb_lignes(le_plateau)
    nb_colonne = matrice.get_nb_colonnes(le_plateau)
    calque = matrice.new_matrice(nb_ligne, nb_colonne, None)
    matrice.set_val(calque, position_depart[0], position_depart[1], 0)
    #rempli calque
    marquage = True
    cpt = 0
    while marquage:
        marquage = False
        #regarde toutes les cases du plateau
        for ligne  in range(nb_ligne):
            # matrice.affiche(calque)
            for colone in range(nb_colonne):
                #si la case n'est pas un mur dans plateau
                if not est_un_mur(le_plateau, (ligne, colone)):
                    #regarde si working_case n'est pas deja marqué
                    if matrice.get_val(calque, ligne, colone) is None:
                        #on regarde toutes les cases voisins de celle en working dans plateau
                        for (lignevoissin, colonnevoisin) in voisins(le_plateau, (ligne, colone)):
                            #si la case voisin de en working contient != None dans le calque
                            if matrice.get_val(calque, lignevoissin, colonnevoisin) is not None:
                                #on defini la nouvelle valeur de working par la val de la case voisine + 1 dans le calque
                                cpt = matrice.get_val(lignevoissin, colonnevoisin) 
                                matrice.set_val(calque, ligne, colone, cpt)
                                cpt += 1
                                marquage = True 
    return calque

def test_fabrique_le_calque():
    le_plateau = init()
    le_calque = fabrique_le_calque(le_plateau, (0, 0))
    matrice.affiche(le_calque)
    assert le_calque == matrice.charge_matrice("./calque1_pour_test.csv")

print(test_fabrique_le_calque())

def fabrique_chemin(le_plateau, position_depart, position_arrivee):
    """Renvoie le plus court chemin entre position_depart position_arrivee

    Args:
        le_plateau (plateau): un plateau de jeu
        position_depart (tuple): un tuple de deux entiers de la forme (no_ligne, no_colonne) 
        position_arrivee (tuple): un tuple de deux entiers de la forme (no_ligne, no_colonne) 

    Returns:
        list: Une liste de positions entre position_arrivee et position_depart
        qui représente un plus court chemin entre les deux positions
    """
    ...


def deplace_fantome(le_plateau, fantome, personnage):
    """déplace le FANTOME sur le plateau vers le personnage en prenant le chemin le plus court

    Args:
        le_plateau (plateau): un plateau de jeu
        fantome (tuple): la position du fantome sur le plateau
        personnage (tuple): la position du personnage sur le plateau

    Returns:
        [tuple]: la nouvelle position du FANTOME
    """
    ...
