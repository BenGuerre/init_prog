def range_betement(liste_cadeau):
    """[summary]

    Args:
        liste_cadeau ([type]): [description]
    """
    taille_working_mal = 0
    traineau = []
    male_in_working = []
    for (nom, place) in liste_cadeau:
        if taille_working_mal + place > 50:
            traineau.append(male_in_working)
            male_in_working = []
            taille_working_mal = 0
        taille_working_mal += place
        male_in_working.append((nom, place))
    traineau.append(male_in_working)
    return traineau
def reverse1(tuple):
    (nom, taille) = tuple
    return (taille, nom)
def range_bien(liste_des_cadeaux):
    """[summary]

    Args:
        liste_des_cadeaux ([type]): [description]
    """
    liste_des_cadeaux2 = sorted(liste_des_cadeaux, key = reverse1)
    male_in_working = []
    traineau = []
    taille_working_mal = 0
    for (nom, place) in liste_des_cadeaux2:
        if taille_working_mal + place > 50:
            traineau.append(male_in_working)
            male_in_working = []
            taille_working_mal = 0
        taille_working_mal += place
        male_in_working.append((nom, place))
    traineau.append(male_in_working)
    return traineau

train = ( "train" , 18)
nours = ("peluche",47)
velo = ("velo", 24)
stylo = ("stylo", 2)
clavier = ("clavier", 25)
console = ("console", 5)
tv = ("tv", 24)
cadeaux_2021 = [ train , nours , velo , stylo , clavier , console , tv ]

def test_range_betement():
    assert range_betement(cadeaux_2021) == [[("train", 18)], [("peluche",47)], [("velo", 24),  ("stylo", 2)], [("clavier", 25),  ("console", 5)],  [("tv", 24)]]

def test_range_bien():
    assert range_bien(cadeaux_2021) == [[("stylo", 2), ("console", 5), ( "train" , 18), ("tv", 24)], [("velo", 24), ("clavier", 25)], [("peluche",47)]]

