# --------------------------------------
# DONNEES
# --------------------------------------

# exemple de liste d'oiseaux observables
OISEAUX=[
        ("Merle","Turtidé"),("Moineau","Passereau"), ("Mésange","Passereau"),  
        ("Pic vert","Picidae"), ("Pie","Corvidé"), ("Pinson","Passereau"),
         ("Rouge-gorge","Passereau"),("Tourterelle","Colombidé")
        ]
# exemples de listes de comptage ces listes ont la même longueur que oiseaux
comptage1=[2,5,0,1,2,0,5,3]
comptage2=[2,1,3,0,0,3,5,1]
comptage3=[0,4,0,3,2,1,4,2]

# exemples de listes d'observations. Notez que chaque liste correspond à la liste de comptage de
# même numéro
observations1=[
        ("Merle",2),  ("Moineau",5), ("Pic vert",1), ("Pie",2), 
        ("Rouge-gorge",5), ("Tourterelle",3)
            ]

observations2=[
        ("Merle",2),("Moineau",1), ("Mésange",3),
        ("Pinson",3),("Rouge-gorge",1), ("Tourterelle",5)
            ]

observations3=[
        ("Mésange",4),("Pic vert",2), ("Pie",2), ("Pinson",1),
        ("Rouge-gorge",4), ("Tourterelle",2)
            ]

# --------------------------------------
# FONCTIONS
# --------------------------------------

def oiseau_le_plus_observe(liste_observations):
    """ recherche le nom de l'oiseau le plus observé de la liste (si il y en a plusieur on donne le 1er trouve)

    Args:
        liste_observations (list): une liste de tuples (nom_oiseau,nb_observes)

    Returns:
        str: l'oiseau le plus observé (None si la liste est vide)
    """
    if len(liste_observations) != 0:
        oiseau_max = liste_observations[0]
        for ind in range(1,len(liste_observations)):
            if liste_observations[ind][1] > oiseau_max[1]:
                oiseau_max = liste_observations[ind]

        return oiseau_max[0]
    return None
print(observations1[1][1]) 
oiseau_le_plus_observe(observations1)

#--------------------------------------
# PROGRAMME PRINCIPAL
#--------------------------------------

#afficher_graphique_observation(construire_liste_observations(oiseaux,comptage3))
#comptage=saisie_observations(oiseaux)
#afficher_graphique_observation(comptage)
#afficher_observations(comptage,oiseaux)

#------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
#Ex2

def recherche_oiseau(nom,liste):
    """Trouve la famill d'un oiseau a partir de son nom

    Args:
        nom (str): le nom de l'oiseau
        liste (list): une liste d'oiseaux avec lerur famille

    Returns:
        [type]: [description]
    """
    if str(nom) :
        for nomoiseaux, famille in liste:
            if nomoiseaux == nom:
                return (nom,famille)
    return None

def recherche_par_famille(famille,liste):
    """donne la liste des oiseaux de la famille entrée en paramètres 

    Args:
        famille (str): le nom de la famille
        liste (liste ): une liste d'oiseaux et leur familles

    Returns:
        [type]: [description]
    """
    if len(liste) != 0:
        if str(famille) :
            res = []
            for ind in range(len(liste)):
                if famille == liste[ind][1]:
                    res.append(liste[ind][0])
        return res
    return None

#------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
#Ex3

def est_liste_observations(liste):
    """Dit si une liste d'observation a ces nom trier dans l'ordre aplha et si il n'y a pas de comptage à 0

    Args:
        liste ([type]): [description]

    Returns:
        [type]: [description]
    """

    if len(liste) != 0:
        for  ind in range(1,len(liste)):
            if liste[ind-1][0] > liste[ind][0] or liste[ind][1] == 10:
                return False
        return True
    return False
est_liste_observations(OISEAUX)

def max_observations(liste):

    if len(liste) != 0:
        res = liste[0]
        for nom, compte in liste:
            if compte > res[1]:
                res = [nom,compte]
        return res[0]
    return None

def moyenne_oiseaux_observes(liste):
    """Donne la moyenne d'oiseaux compté

    Args:
        liste (lsite): une mliste de comptage

    Returns:
        float: la moyenne 
    """
    if len(liste) != 0:
        res = 0
        for ind in range(len(liste)):
            res += liste[ind][1]
        return res / len(liste)
    return None

def total_famille(listeob,listeoiseaux,famille):
    """calcule le nombre total de spécimens observés pour une famille d’oiseaux 

    Args:
        listeob (list): la liste de d'oservation
        listeoiseaux (list): une liste de 
        famille (str): le nom de la famille dont nous voulons obtenir le nombre
    Returns:
        str: donne le nombre d'oiseau de la famille vue 
    """
    res = 0
    if est_liste_observations(listeob):
        famille_nom = recherche_par_famille(famille,listeoiseaux)
        for nom in famille_nom:
            for nom_ob,compte_ob in listeob:
                if nom == nom_ob:
                    res += compte_ob
    return res


#------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
#Ex4

def construire_liste_observationse(liste_oiseaux,liste_comptage):
    """Creer une liste de comptage

    Args:
        liste_oiseaux (list): Une liste d'oiseaux
        liste_comptage (list): Une liste de comptage

    Returns:
        list Une liste d'obseervation
    """
    liste_ob = []
    for ind in range(len(liste_oiseaux)):
        if liste_comptage[ind] != 0:
            liste_ob.append((liste_oiseaux[ind][0],liste_comptage[ind]))
    if  est_liste_observations(liste_ob):
        return liste_ob 
    return []
                

def creer_ligne_sup(liste_oiseaux):
    """Créer un liste de d'observation

    Args:
        liste_oiseaux (list): Une liste d'oiseaux

    Returns:
        list: Une liste d'observation
    """
    liste_compt = []
    for nom,famille in liste_oiseaux:
        a = input("Donnez le nombre de " + nom + " vue")
        if not a.isdecimal():
            a = input("Donnez le nombre de " + nom + " vue(attenion cela doit être un nombre)")
        if int(a) != 0:
            liste_compt.append((nom,a))
    return liste_compt

print(creer_ligne_sup(OISEAUX))