# exercice 2

from abc import abstractclassmethod


def plus_moins_pair(entree):
    """
    Donne si il y plus de nombre pair ou impaire dans la liste entree   

    Args:
        entree (list): Une liste de nombre

    Returns:
        (bool): True si il y a plus de nombre pair ou autant et False si plus de nombre impair
    """
    compte_pair=0
    compte_impair=0
    # au début de chaque tour de boucle
    # xxx contient le nombre de nombre pair déjâ vue  dans la liste entree
    # yyy contient le nombre de nombre impaire déjâ vue  dans la liste entree
    for elmt in entree:
        if elmt%2==0:
            compte_pair+=1
        else:
            compte_impair+=1
    return compte_pair>=compte_impair


def test_plus_moins_pair():
    assert plus_moins_pair([1,4,6,-2,-5,3,10]) == True
    assert plus_moins_pair([-4,5,-11,-56,5,-11]) == False 
    assert plus_moins_pair([10,11,12,13]) == True
    assert plus_moins_pair([]) == True



  
    

 

 



# exercice 3

#3.1) La fonction ne fonctionne pas toujours car on initialise la vasiable res avec le 1er nb de la  liste qui peut être plus petit que valeur

def min_sup(liste_nombres,valeur):
    """trouve le plus petit nombre d'une liste supérieur à une certaine valeur

    Args:
        liste_nombres (list): la liste de nombres
        valeur (int ou float): la valeur limite du minimum recherché

    Returns:
        int ou float: le plus petit nombre de la liste supérieur à valeur
    """
    # au début de chaque tour de boucle res est le plus petit élément déjà énuméré
    # supérieur à valeur
    res = None 
    for elem in liste_nombres:
        if elem > valeur:
            if res == None:
                res = elem
            else:
                if elem < res:
                    res = elem
    return res
 


def test_min_sup():
    assert min_sup([8,12,7,3,9,2,1,4,9],5)==7
    assert min_sup([-2,-5,2,9.8,-8.1,7],0)==2
    assert min_sup([5,7,6,5,7,3],10)==None
    assert min_sup([],5)==None
    














# exercice 4
def nb_mots ( phrase ) :
    """Fonction qui compte le nombre de mots d'une phrase

    Args:
        phrase (str): une phrase dont les mots sont séparés par des espaces (éventuellement plusieurs)

    Returns:
        int: le nombre de mots de la phrase
    """    
    resultat =0
    #initialiser avec un espace
    precedent = ' '
    # au début de chaque tour de boucle
    # c1 vaut dernier caractère traité
    # c2 vaut nouveau caractère traité
    # resultat vaut le nombre de mots vue
    for lettre in phrase :
        if precedent == ' ' and lettre != ' ':
            resultat += 1
        precedent = lettre
    return resultat 

nb_mots("bonjour, il fait beau")
def test_nb_mots():
    assert nb_mots("bonjour, il fait beau")==4
    assert nb_mots("houla!     je    mets beaucoup   d'  espaces    ")==6
    assert nb_mots(" ce  test ne  marche pas ")==5
    assert nb_mots("")==0 #celui ci non plus
    
    

def somme_paire(liste):
    """Donne la somme des nombres pair de la liste

    Args:
        liste (liste): une liste d'entier

    Returns:
        int : la somme des nombres pairs
    """    
    somme = None
    #elmt contient la valeur étudier
    #Somme contient la somme des nombres paire déjà traité
    for elmt in liste:
        if elmt % 2 == 0:
            if somme == None:
                somme = elmt
            else:
                somme += elmt
        
    return somme
            
def test_somme_paire():
    assert somme_paire([12,13,6,5,7]) == 18
    assert somme_paire([]) == None
    assert somme_paire([1,7,99]) == None
    assert somme_paire([100,-200]) == -100


    

def der_voyelle(chaine):
    """Donne la dernière voyelle de la chaine de caractère

    Args:
        chaine (str): une phrase ou un mot

    Returns:
        str: La dernière voyelle de la chaine de caractère
    """    
    res = None
    #res contient la dernièere voyelle vue
    for elmt in chaine:
        if elmt in 'aeiouyAEIOUY':
            res = elmt 
    return res

def test_der_voyelle():
    assert der_voyelle('bonjour') == 'u'
    assert der_voyelle("buongiorno") == 'o'
    assert der_voyelle('') == None
    assert der_voyelle('pqrst') == None
    assert der_voyelle('athgx') == 'a'




def nb_negatif(liste):
    """Donne le nombre de nombres négatif

    Args:
        liste (list): Une liste de nombres

    Returns:
        float: le nombre de négatifs
    """    
    negatif = 0
    compteur = 0
    res = None
    #negatif a le nombres de négatifs dans la liste
    #compteur compte le nombre de nombre dans la liste
    #res contient None
    #elmt est l'élément de la liste que l'on traite
    for elmt in liste:
        if elmt < 0 :
            negatif += 1
        compteur += 1
    #si il y a un ou des nombres négatifs, on calcul la proportion dans res
    if negatif != 0:
        res = negatif / compteur
    return res

def test_nb_negatif():
    assert nb_negatif([]) == None
    assert nb_negatif([1,5,8,9]) == None
    assert nb_negatif([4,-2,8,2,-2,-7]) == 0.5
    assert nb_negatif([-7,-4,54,-78,45]) == 0.6
#Exercice 6 
def somme_entiers(n):
    """Fait la somme des n premiers entiers

    Args:
        n (int): Le nombres des premiers entiers

    Returns:
        int: la somme des n premiers entiers
    """  
    somme = 0
    #sommme contient la somme des nombres deja vue
    #elmt est l'élément est le nombre que l'on va jouter
    for elmt in range(n+1):
        somme += elmt

    return somme

def test_somme_entiers():
    assert somme_entiers(4) == 10
    assert somme_entiers(0) == 0
    assert somme_entiers(2) == 3
    assert somme_entiers(1) == 1


def suite_syracuse(n,val_init):
    """calcule le rang n d'une suite 
    Args:
        n(int) : rang de la suite qu el'on veut calculer
        val_init (int) : valeur d'initialisation de la suite
    Return:
        u (float) : correspond a la valeur de la suite au rang n selon val_init
    """
    u=0
    #cette boucle va faire les calcul de la suite de syracuse jusqu'a la valeur choisi
    for rang in range(0,n+1):
        if rang == 0 :
            u = val_init
        elif u % 2 == 0 :
            u = u / 2
        else:
            u = 3 * u + 1
    return u

print(suite_syracuse(7,3))
def test_suite_syracuse():
    assert suite_syracuse(2,6)==10.0
    assert suite_syracuse(0,1)==1.0
    assert suite_syracuse(7,3)==1.0
    assert suite_syracuse(4,0)==0

#Ex7

def somme(liste):
    """Donne la somme des nombre de la liste

    Args:
        liste (list): Une liste de nombre

    Returns:
        int: La somme des nombres de la liste
    """    
    res = None
    #valeur est l'élement a ajouter a la liste
    for valeur in liste:
        if res == None:
            res = valeur
        else:
            res +=  valeur

    return res

def test_somme():
    assert somme([]) == None
    assert somme([4,-4]) == 0
    assert somme((1,2,3)) == 6
    assert somme([10,20,30]) == 60

def min(liste):
    """Donne le miinimum d'une liste

    Args:
        liste (list): Une liste de nombres

    Returns:
        int: redonne le plus petit nombre
    """
    minimun = None
    #elmt est l'élément étudié
    for elmt in liste:
        if minimun == None or minimun > elmt :
            minimun =elmt
    return minimun

def test_min():
    assert min([1,2,3,4]) == 1
    assert min([]) == None
    assert min([8,-4,5,10,0]) == -4
    assert min([1]) == 1


def max(liste):
    """Donne le maximum d'une liste

    Args:
        liste (list): Une liste de nombres

    Returns:
        int: redonne le plus grand nombre
    """
    maxi = None
    #elmt est l'élément étudié
    for elmt in liste:
        if maxi == None or maxi < elmt :
            maxi =elmt
    return maxi

def test_maxi():
    assert max([1,2,3,4]) == 4
    assert max([]) == None
    assert max([8,-4,5,10,0]) == 10
    assert max([1]) == 1

def difference(liste):
    """Redonne la difference entre le plus grand et le plus plus petit

    Args:
        liste (list): une liste d'entier

    Returns:
        int: la difference entre le plus grand et le plus petit
    """
    max = max(liste)
    min = min(liste)
    diff = None
    if max or min != None:
        diff = max-min
    return diff

def test_difference():
    assert difference([4,8,7,5,1]) == 7
    assert difference([]) == 0
    assert difference([1]) == 0
    assert difference([-4,8,4,2,1]) == 12

