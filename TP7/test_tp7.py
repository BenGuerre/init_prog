    #"""Fonctions de test du TP7
    #ATTENTION VOUS DEVEZ COMPLETER LES TESTS
    #pour appeler vos fonctions ecrivez: tp7_source.ma_fonction
    #"""
import tp7_source

liste_communes= [   ('18001','aaa',1200),('18002','bbb',71200),('18003','ccc',520),
                    ('45001','ddd',85200),('45002','abcd',6350),('45003','aaa sur Loire',8534),('45004','ggg',5201)]

def test_charger_fichier_population():
    assert tp7_source.charger_fichier_population("extrait1.csv") == [(45232, 'Olivet', 22474),
    (45233, 'Ondreville-sur-Essonne', 416),
    (45234, 'Orléans', 119085),
    (45235, 'Ormes', 4200),
    (51204, 'Damery', 1464),
    (51205, 'Dampierre-au-Temple', 279),
    (51206, 'Dampierre-le-Château', 108),
    (51208, 'Dampierre-sur-Moivre', 115),
    (77345, 'Orly-sur-Morin', 688),
    (77347, 'Les Ormes-sur-Voulzie', 868),
    (77348, 'Ormesson', 249)]


def test_population_d_une_commune():
    assert tp7_source.population_d_une_commune(liste_communes , 'ccc')==520
    assert tp7_source.population_d_une_commune(liste_communes , "tourcoin") is None
    assert tp7_source.population_d_une_commune([] , "Tour") is None

def test_liste_des_communes_commencant_par():
    assert tp7_source.liste_des_communes_commencant_par(liste_communes,'ab') == ['abcd']

def test_commune_plus_peuplee_departement():
    assert tp7_source.commune_plus_peuplee_departement(liste_communes,18) == 'bbb'

def test_nombre_de_communes_tranche_pop():
    ...

def test_ajouter_trier():
    ...
    

def test_top_n_population():
    ...

def test_population_par_departement():
    ...
