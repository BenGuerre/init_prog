   def menu_principal():
    """permet d'afficher le menu principal
    Return:
        l'affichage du menu
    """
    fenetre = Tk() #permet de créer un fenetre
    fenetre.title('Menu Principal') #donne un nom a la fenetre
    label = Label(fenetre , text = "Menu Principal")
    label.pack()# met a jour le contenu

    fenetre.mainloop()
    

def programme_principal():
    menu_principal()

print(programme_principal())


 #TP7 une application complète
   # ATTENTION VOUS DEVEZ METTRE DES DOCSTRING A TOUTES VOS FONCTIONS

liste_communes= [   ('18001','aaa',1200),('18002','bbb',71200),('18003','ccc',520),
                    ('45001','ddd',85200),('45002','abcd',6350),('45003','aaa sur Loire',8534),('45004','ggg',5201)]


def afficher_menu(titre, liste_options):
    """Permet d'affcher un menu

    Args:
        titre (str): Le titre du menu
        liste_options (list): Une list dEs options en str
    """
    choix = ""
    print("+-------------------------+")
    print("| " +  titre + " |")
    print("+-------------------------+")
    for ind in range(len(liste_options)):
        print(str(ind+1) + " -> " + str(liste_options[ind]))

def demander_nombre(message, borne_max):
    """Demande un nombre

    Args:
        message (str): le message à afficher
        borne_max (int): le nombre de posssibillité

    Returns:
        int: la demande
    """
    affiche = 0
    affiche = input(message + "[" + "1" + "-" + str(borne_max) + "]")
    try:
        if int(affiche) in range(1,borne_max+1): 
            return(int(affiche))
    except:
        return None

def menu(titre,liste_options):
    """affiche le menu

    Args:
        titre (str): le titre du menu 
        liste_options (list): une liste des différentes options
    """
    afficher_menu(titre, liste_options)
    return(demander_nombre("Entrez votre choix", len(liste_options)))


def programme_principal():
    liste_options=["Charger un fichier","Rechercher la population d'une commune","trouver une commune","Afficher la population d'un département","Tranche population","Quitter"]
    liste_communes=[]
    while True:
        rep=menu("MENU DE MON APPLICATION", liste_options)
        if rep is None:
            print("Cette option n'existe pas")
        elif rep==1:
            nom_fic = input("nom du fichier à charger ")
            print("il y a " + str(len(charger_fichier_population(nom_fic))) + " communes trouvée")

        elif rep==2:
            nom_fic = input("nom du fichier à charger ")
            nom_commune = input("entrez le nom de la commune ")
            print("il y a  " + str(population_d_une_commune(charger_fichier_population(nom_fic) , nom_commune)) + " Habitants dans " + nom_commune)
        elif rep==3:
            nom_fic = input("nom du fichier à charger ")
            nom_commune = input("entrez le debut du nom de la commune ")
            print("il y a  " + liste_des_communes_commencant_par(charger_fichier_population(nom_fic) , nom_commune) + " commançant par " + nom_commune)
        elif rep==4:
            nom_fic = input("nom du fichier à charger ")
            num_dep = input("entrez le numero du departement ")
            print( "la commune la plus peuplé du" + num_dep + " est " + commune_plus_peuplee_departement(charger_fichier_population(nom_fic) , int(num_dep)))
        elif rep == 5:
            nom_fic = input("nom du fichier à charger ")
            min_pop = input("entrez lz minimum")
            max_pop = input("entrez lz mzximum")
            print( "il y a " + str(nombre_de_communes_tranche_pop(charger_fichier_population(nom_fic) , int(min_pop), int(max_pop))) + "dans la tranche demandé")
        else:
            break
        input("Appuyer sur Entrée pour continuer")
    print("Merci au revoir!")



#2.1
def charger_fichier_population(nom_fic):
    """produit une liste de tuple avec DEPCOM,COM,PTOT

    Args:
        nom_fic (str): nom du fichier

    Returns:
        liste: la liste des tuples
    """
    res=[]
    fic = open(nom_fic, 'r') #lit ce qu'il y a dans le fichier
    fic.readline()#permet de juste lire la premiere ligne et r faire
    for ligne in fic:
        ligne_traité = ligne.split(";")
        res.append((ligne_traité[0], str(ligne_traité[1]),int(ligne_traité[4])))
    fic.close()
    return res 



def population_d_une_commune(liste_pop,nom_commune):
    """Permet de rechercher la population d'une commune dans un liste

    Args:
        liste_pop (list): liste de population
        nom_commune (str): La commune recherché dans la liste

    Returns:
        int (or None): le nombre d'habitant de la commune (ou None si n'est pas dans la liste)
    """
    for (_ ,nom_commune_liste ,pop) in liste_pop:
        if nom_commune == nom_commune_liste:
            return pop
    return None

#2.4
def liste_des_communes_commencant_par(liste_pop, debut_nom):
    """retrouve dans un liste les noms des communes commancant par debut_nom

    Args:
        liste_pop (list): Une liste de population
        debut_nom (str): le début de nom

    Returns:
        list: La liste des communes commençant par debut_nom
    """
    res =  []
    for (_ ,nom_commune_liste , _) in liste_pop:
        peut_ajouter = True
        for ind in range(len(debut_nom)):
            if nom_commune_liste[ind] != debut_nom[ind]:
                peut_ajouter = False
        if peut_ajouter:
            res.append(nom_commune_liste)
    return res
#Ex2.5
def commune_plus_peuplee_departement(liste_pop, num_dpt):
    """prermet de retrouver la liste des noms de commune qui commencent par
une certaine chaîne de caractères dans une liste

    Args:
        liste_pop (list): une liste de population
        num_dpt (int): le numero du département

    Returns:
        str: le nom de la commune la plus peuplé du département
    """
    res = ()
    dans_dep = []
    for (depcom , nom_commune_liste , pop) in liste_pop:
        if str(depcom[0]) + str(depcom[1]) == str(num_dpt):
            dans_dep.append((nom_commune_liste,pop))
    for (nom_commune_liste,pop) in dans_dep:
        if res == ():
            res = (nom_commune_liste,pop)
        if pop > res[1]:
            res = (nom_commune_liste,pop)
    return res[0]
#2.6
def nombre_de_communes_tranche_pop(liste_pop, pop_min,pop_max):
    """permet de retourner le nombre de communes qui appartiennent à une
certaine tranche de population.

    Args:
        liste_pop (list): liste de population
        pop_min (int): le nombre minimum de pop
        pop_max (int): le nombre max de pop

    Returns:
        int: le nombre de commune dans la tranche
    """
    res = 0
    for (_ , _ , pop) in liste_pop:
        if pop >= pop_min and pop <= pop_max:
            res += 1 
    return res

#2.7
def place_top(commune, liste_pop):
    ...

def ajouter_trier(commune,liste_pop, taille_max):
    ...
    

def top_n_population(liste_pop, nb):
    ...

def population_par_departement(liste_pop):
    ...

def sauve_population_dpt(nom_fic, liste_pop_dep):
    ...

# appel au programme principal
programme_principal()
