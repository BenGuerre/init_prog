# Codé par Papy Force X, jeune padawan de l'informatique

# def dialogue_mot_de_passe():
#     login = input("Entrez votre nom : ")
#     mot_de_passe_correct = False
#     while not mot_de_passe_correct:
#         mot_de_passe = input("Entrez votre mot de passe : ")
#         # je vérifie la longueur
#         if len(mot_de_passe) < 8:
#             longueur_ok = False;
#         else:
#             longueur_ok = True
#         # je vérifie s'il y a un chiffre
#         chiffre_ok = False
#         for lettre in mot_de_passe:
#             if lettre.isdigit():
#                 chiffre_ok = True
#         # je vérifie qu'il n'y a pas d'espace
#         sans_espace = True
#         for lettre in mot_de_passe:
#             if lettre == " ":
#                 sans_espace = False
#         # Je gère l'affichage
#         if not longueur_ok:
#             print("Votre mot de passe doit comporter au moins 8 caractères")
#         elif not chiffre_ok:
#             print("Votre mot de passe doit comporter au moins un chiffre")
#         elif not sans_espace:
#             print("Votre mot de passe ne doit pas comporter d'espace")	   
#         else:
#             mot_de_passe_correct = True        
#     print("Votre mot de passe est correct")
#     return mot_de_passe

# dialogue_mot_de_passe()

def longueur_ok(mot_de_passe):
    """dit si la longeur du mot de passe est bonne

    Args:
        mot_de_passe (str): le mot de passe

    Returns:
        bool: True si la longeur est bonne et False si la longueur n'est pas bonne  
    """
    return len(mot_de_passe) >= 8

def chiffre_ok(mot_de_passe):
    """Dit si mot de passe comporte un chiffre

    Args:
        mot_de_passe (str): le mot de passe

    Returns:
        bool: True si cintient un chiffre False si ne comtient pas de chiffre
    """
    ind = 0
    res = False
    while ind  < len(mot_de_passe) and not res: # s'arrete ind >= len(mot_de_passe) ou quand trouve un chiffre
        res = mot_de_passe[ind].isdigit()  
        ind += 1   
    return res

def sans_espace(mdp):
    """dit si le mot de passe ne contient pas un espace

    Args:
        mdp (str): le mot de passe

    Returns:
        bool : True si n'en a pas False si il y  en à un
    """
    ind = 0
    sans_space = True
    while ind < len(mdp) and sans_space: #s'arrete quand trouve un mot de passe ou ind >= len(mdp)
        sans_space = mdp[ind] != " "
        ind += 1
    return sans_space


def dialogue_mot_de_passe_v2():
    login = input("Entrez votre nom : ")
    mot_de_passe_correct = False
    while not mot_de_passe_correct:
        mot_de_passe = input("Entrez votre mot de passe : ")
       
        # Je gère l'affichage
        #longueur
        if not longueur_ok(mot_de_passe):
            print("Votre mot de passe doit comporter au moins 8 caractères")
        #chiffre
        elif not chiffre_ok(mot_de_passe):
            print("Votre mot de passe doit comporter au moins un chiffre")
        #espaces
        elif not sans_espace(mot_de_passe):
            print("Votre mot de passe ne doit pas comporter d'espace")	   
        else:
            mot_de_passe_correct = True        
    print("Votre mot de passe est correct")
    return mot_de_passe

# dialogue_mot_de_passe_v2()

def chiffre_ok_v2(mdp):
    """dit si le mot de passe contient au moins trois chiffre 

    Args:
        mdp (str): le mot de passe
    Returns:
        bool : True si conntient trois chiffres ou plus False si ne contient pas trois chiffres
    """
    nb_chiffre = 0
    ind = 0
    while ind < len(mdp) and nb_chiffre < 3: #s'arrete: ind >= len(mdp) ou nb_chiffre = 3
        if mdp[ind].isdigit():
            nb_chiffre += 1
        ind += 1
    return nb_chiffre >= 3

def chiffre_non_consécutifs(mdp):
    """Dit si le mot de passe ne comprte pas dux chiffres consécutifs
    Args:
        mdp(str) : le mot de passe
    Returns:
        bool: True si ne contient pas deux chiffres consécutif False si consécutifs
    """
    ind = 0
    consecutif = 0
    while ind < len(mdp) and consecutif != 2:  #s'arrete: ind >= len(mdp) ou consecutif = 2:
        if mdp[ind].isdigit():
            consecutif += 1
        else:
            consecutif = 0
            ind += 1
    return consecutif < 2