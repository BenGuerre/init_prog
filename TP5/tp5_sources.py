#Ex1

#quest1:   xxx contient le nombre d'élément de la liste != de val déjà vue
#          yyy contient le nombre d'élement de la liste == à val déjà vue avnt de la voiir x4

#quest2:    return xxx est éxécuter si yyy > 3

def recherche(liste,valeur):
    """Calcule si il 3 fois val dans la liste et combien de valeur avant le troisième

    Args:
        liste ([list]): une liste de réel
        valeur ([int]): Un entier

    Returns:
        None or int: si il y a 4x val Redonne le nombre d'élément avant le troisième val
    """
    nb_fois_elmt = 0
    for indice in range(len(liste)):
        if liste[indice] == valeur:
            nb_fois_elmt += 1
            if nb_fois_elmt > 3:
                return indice 
    return None

def test_mystere():
    assert recherche([12,5,8,48,12,418,185,17,5,87],20) == None
    assert recherche([1,8,8,8,6,8,4],8) == 5
    assert recherche([],8) == None
    assert recherche([1,1,1,1],1) == 3

# --------------------------------------
# Exemple de villes avec leur population
# --------------------------------------
liste_villes = ["Blois","Bourges","Chartres","Châteauroux","Dreux","Joué-lès-Tours","Olivet","Orléans","Tours","Vierzon"]
population = [45871, 64668,  38426, 43442, 30664, 38250, 22168, 116238, 136463,  25725]

#---------------------------------------
# Exemple de scores
#---------------------------------------
scores=[352100,325410,312785,220199,127853]
joueurs=['Batman','Robin','Batman','Joker','Batman']

#Ex2
def ind_chif(chaine):
    """Trouve premier chiffre dans une chaine de cararctère 

    Args:
        chaine (str): Une prase

    Returns:
        None or int: None si il n'y a pas de nombre ou l'indice du nombre dans  la chaine de caractère
    """
    for indice in range(len(chaine)):
        if chaine[indice] in ["0","1","2","3","4","5","6","7","8","9"]:
            return indice
    return None

def test_ind_chif():
    assert ind_chif("on est le 30/09/2021" ) == 10
    assert ind_chif("o3n est le 30/09/2021" ) == 1
    assert ind_chif("" ) == None
    assert ind_chif("bonjour, a tous!" ) == None

def recherche_habitant(liste_ville,liste_hab,ville):
    """Trouve le nombres d'habitants de la ville ville

    Args:
        liste_ville (list): liste avec le nom des villes
        liste_hab (list): liste avec le nombre d'habitant
        ville (str): nom de la ville recherchée

    Returns:
        None or int: None si le les deux liste ne sont pas de la mếme taille ou si la ville n'est pas dans la liste OU le nombre d'habitant de la ville 
    """    
    if len(liste_ville) != len(liste_hab):#regarde si les deux listes sont de même taille
        return None
    for indice in range(len(liste_ville)):
        if liste_ville[indice] == ville:
            return liste_hab[indice]
    return None

def test_recherche_habitant():
    assert recherche_habitant(liste_villes,population,"Châteauroux") == 43442
    assert recherche_habitant(liste_villes,[],"Châteauroux") == None
    assert recherche_habitant([],population,"Châteauroux") == None
    assert recherche_habitant(liste_villes,population,"Tourcoin") == None

#Ex3
def verif_tri_croissant(liste):
    """Regarde si la liste est trié dans l'ordre croissant

    Args:
        liste (list): Une lmiste de nombres

    Returns:
        bool ou None: si la liste est trié dans l'ordre croissant ou non OU None si vide
    """
    if liste == []:
        return None
    else:
        for indice in range(1,len(liste)):
            if liste[indice] < liste[indice - 1]:
                return False
    return True

def test_verif_tri_croissant():
    assert verif_tri_croissant([1,2,3,4,5,6]) 
    assert verif_tri_croissant([]) == None
    assert not verif_tri_croissant([1,2,3,1,4,6]) 
    assert verif_tri_croissant([5]) 

def seuil(liste,seuil):
    """regarde si omme de la liste depasse seuil

    Args:
        liste (list): Une liste d'entier
        seuil (int): in Nombre

    Returns:
        bool: True si depasse sueil False si ne le dépasse pas
    """    
    somme = 0
    for elmt in liste:
        somme += elmt
        if somme > seuil:
            return True
    return False

def test_seuil():
    assert seuil([1,4,1,2,3,4],6)
    assert not seuil([],8) 
    assert not seuil((-1,-8,-7),10) 
    assert not seuil([-1,-5,-8],-1)

def mailverif(mail):
    """verifi si l'e-maill est correct

    Args:
        mail (str): un e-mail

    Returns:
        bool: True si le mail est correct False ssi incorect
    """    
    aro = 0
    for ind in range(len(mail)):
        if mail[ind].isalpha or mail[ind] == '@' or '.' :
            if mail[ind] == '@' and aro == 1:
                return False

            if mail[ind] == '@' and aro == 0:
                aro = 1

        else:
            return False
    return True

'''def test_mailverif():
    assert mailverif('karim.ka@mo.bo') 
    assert not mailverif(' karim.ka@mo.bo') 
    assert not mailverif('kargim.ka@bo.mo')
    assert not mailverif('kargim.ka@bomo')
'''




















#Ex4

lscores = [352100,325410,312785,220199,127853]
ljoueurs = ['Batman','Robin','Batman','Joker','Batman']

def best_score(scores,joueurs,joueur):
    """Donne le meilleur svcore d'un joueura

    Args:
        scores (list): liste de score
        joueurs (list): liste des joueurs
        joueur (str): nom du joueur que l'on veut le score

    Returns:
        int: Le score du joueur
    """    
    if len(scores) != len(joueurs):
        return None
    for ind in range(len(joueurs)):
        if joueurs[ind] == joueur:
            return scores[ind]
    return None

def test_best_score():
    assert best_score(lscores,ljoueurs,'Batman') == 352100
    assert best_score([],['Batman'],'Batman') == None
    assert best_score(lscores,['Robin','Batman'],'Batman') == None
    assert best_score(lscores,ljoueurs,'bob') == None

def verif_tri_decroissant(liste):
    """Regarde si la liste est trié dans l'ordre decroissant

    Args:
        liste (list): Une lmiste de nombres

    Returns:
        bool ou None: si la liste est trié dans l'ordre decroissant ou non OU None si vide
    """
    if liste == []:
        return None
    else:
        for indice in range(1,len(liste)):
            if liste[indice] > liste[indice - 1]:
                return False
    return True

def test_verif_tri_decroissant():
    assert verif_tri_decroissant(lscores) 
    assert verif_tri_decroissant([]) is None
    assert not verif_tri_decroissant([5,4,9]) 
    assert  verif_tri_decroissant([9]) 

def nb_meilleur_score(l_joueurs,nom_joueur):
    """Donne combiende fois un joueur apparait dans les meilleurs scores

    Args:
        l_joueurs (list): Liste des noms des joueurs avec les meilleurs scores
        nom_joueur (str): nom du joueur rechercher

    Returns:
        int : nombre de fois que le joueur apparaît 
    """
    if len(l_joueurs) == 0 or nom_joueur == "":
        return 0
    res = 0
    for elmt in l_joueurs:
        if elmt == nom_joueur:
            res +=1
    return res

def test_nb_meilleur_score():
    assert nb_meilleur_score(ljoueurs,"Batman") == 3
    assert nb_meilleur_score(["aaa","bbb"],"jjj") == 0
    assert nb_meilleur_score([],"jjj") == 0
    assert nb_meilleur_score(["aaa","bbb"],"") == 0

def best_classement(l_joueurs,l_score,joueur):
    """Donne lbest_classement(ljoueurs,lscores,"Batman")e meilleur classement d'un joueur

    Args:
        l_joueurs (liste): liste des noms des joueur
        l_score (list): liste des scores
        joueur (str): nom du joueur
     Returns:
        int ou none : La place dans le classement ou none si liste vide ou son nom n'apparait pas    
    """
    if len(l_joueurs) or len(l_score) != 0: #verif si les listes ne sont pas vide
        if verif_tri_decroissant(l_score): #verif si liste des score dans ordre croissant
            for ind in range(len(l_joueurs)):
                if l_joueurs[ind] == joueur:
                    return ind + 1 
    return None
best_classement(ljoueurs,lscores,"Batman")
def test_best_classement():
    assert best_classement(ljoueurs,lscores,"Batman") == 1
    assert best_classement(ljoueurs,lscores,"Karim") is None
    assert best_classement(ljoueurs,[],"Batman") is None    
    assert best_classement([],lscores,"Batman") is None


def ind_insert_score(score,l_score):
    """Donne l'indice d'insertion du score dans la liste de score pour quel reste dans l'ordre décroissant

    Args:
        score (int): le score que l'on souhaite ins"rer
        l_score (list): Une liste de scores

    Returns:
        int: L'indice où devrait s'insérer le score 
    """
    resind = None
    if verif_tri_decroissant(l_score):#verif si liste des score dans ordre croissant
        for ind in range(len(l_score)):
            if l_score[ind] > score or ind == len(l_score):
                resind = ind + 1 
        if resind is None:
            resind = 0
    return resind
def test_ind_insert_score():
    assert ind_insert_score(1,[4,3,2]) == 3
    assert ind_insert_score(1,[-1,3,2]) is None
    assert ind_insert_score(1,[]) is None
    assert ind_insert_score(5,[4,2]) == 0


#--------------------------------------------------------------------------------------------------
def add_nv_score(score,nom,l_score,l_joueurs):
    ind = ind_insert_score(score,l_score)
    if ind is None:
        return None
    l_score.insert(ind,score)
    l_joueurs.insert(ind,nom)

def test_add_nv_score():
    scores = [89,4]
    joueurs = ["bob","jack"]
    add_nv_score(10,"karim",scores,joueurs)
    assert scores == [89,10,4]
    assert joueurs == ["bob","karim","jack"]



#sans return la fonction est "en l'état"

#on ajoute des variable locale a la fonction test
#Pour verif on appel la fonction avec en param les variables locale de la fonction test puis assert var local de la fonction test == ce que sa doit donné aprés
