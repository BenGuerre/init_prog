   # """Fichier source de la SAE 1.01 partie 1
   # Historique des matchs de football internationaux
   # """

# ---------------------------------------------------------------------------------------------
# Exemples de données pour vous aidez à faire des tests
# ---------------------------------------------------------------------------------------------
    
# exemples de matchs de foot
match1=('2021-06-28', 'France', 'Switzerland', 3, 3, 'UEFA Euro', 'Bucharest', 'Romania', True)
match2=('1998-07-12', 'France', 'Brazil', 3, 0, 'FIFA World Cup', 'Saint-Denis', 'France', False)
match3=('1978-04-05', 'Germany', 'Brazil', 0, 1, 'Friendly', 'Hamburg', 'Germany', False)

#exemples de liste de matchs de foot
liste1=[('1970-04-08', 'France', 'Bulgaria', 1, 1, 'Friendly', 'Rouen', 'France', False), 
        ('1970-04-28', 'France', 'Romania', 2, 0, 'Friendly', 'Reims', 'France', False), 
        ('1970-09-05', 'France', 'Czechoslovakia', 3, 0, 'Friendly', 'Nice', 'France', False), 
        ('1970-11-11', 'France', 'Norway', 3, 1, 'UEFA Euro qualification', 'Lyon', 'France', False)
        ]
liste2=[('1901-03-09', 'England', 'Northern Ireland', 3, 0, 'British Championship', 'Southampton', 'England', False), 
        ('1901-03-18', 'England', 'Wales', 6, 0, 'British Championship', 'Newcastle', 'England', False), 
        ('1901-03-30', 'England', 'Scotland', 2, 2, 'British Championship', 'London', 'England', False), 
        ('1902-05-03', 'England', 'Scotland', 2, 2, 'British Championship', 'Birmingham', 'England', False), 
        ('1903-02-14', 'England', 'Northern Ireland', 4, 0, 'British Championship', 'Wolverhampton', 'England', False), 
        ('1903-03-02', 'England', 'Wales', 2, 1, 'British Championship', 'Portsmouth', 'England', False), 
        ('1903-04-04', 'England', 'Scotland', 1, 2, 'British Championship', 'Sheffield', 'England', False), 
        ('1905-02-25', 'England', 'Northern Ireland', 1, 1, 'British Championship', 'Middlesbrough', 'England', False), 
        ('1905-03-27', 'England', 'Wales', 3, 1, 'British Championship', 'Liverpool', 'England', False), 
        ('1905-04-01', 'England', 'Scotland', 1, 0, 'British Championship', 'London', 'England', False), 
        ('1907-02-16', 'England', 'Northern Ireland', 1, 0, 'British Championship', 'Liverpool', 'England', False), 
        ('1907-03-18', 'England', 'Wales', 1, 1, 'British Championship', 'London', 'England', False), 
        ('1907-04-06', 'England', 'Scotland', 1, 1, 'British Championship', 'Newcastle', 'England', False), 
        ('1909-02-13', 'England', 'Northern Ireland', 4, 0, 'British Championship', 'Bradford', 'England', False), 
        ('1909-03-15', 'England', 'Wales', 2, 0, 'British Championship', 'Nottingham', 'England', False), 
        ('1909-04-03', 'England', 'Scotland', 2, 0, 'British Championship', 'London', 'England', False)
        ]
liste3=[('1901-03-30', 'Belgium', 'France', 1, 2, 'Friendly', 'Bruxelles', 'Belgium', False),
        ('1901-03-30', 'England', 'Scotland', 2, 2, 'British Championship', 'London', 'England', False),
        ('1903-04-04', 'Brazil', 'Argentina', 3, 0, 'Friendly', 'Sao Paulo', 'Brazil', False),
        ('1903-04-04', 'England', 'Scotland', 1, 2, 'British Championship', 'Sheffield', 'England', False), 
        ('1970-09-05', 'France', 'Czechoslovakia', 3, 0, 'Friendly', 'Nice', 'France', False), 
        ('1970-11-11', 'France', 'Norway', 3, 1, 'UEFA Euro qualification', 'Lyon', 'France', False)
        ]
liste4=[('1978-03-19', 'Argentina', 'Peru', 2, 1, 'Copa Ramón Castilla', 'Buenos Aires', 'Argentina', False), 
        ('1978-03-29', 'Argentina', 'Bulgaria', 3, 1, 'Friendly', 'Buenos Aires', 'Argentina', False), 
        ('1978-04-05', 'Argentina', 'Romania', 2, 0, 'Friendly', 'Buenos Aires', 'Argentina', False), 
        ('1978-05-03', 'Argentina', 'Uruguay', 3, 0, 'Friendly', 'Buenos Aires', 'Argentina', False), 
        ('1978-06-01', 'Germany', 'Poland', 0, 0, 'FIFA World Cup', 'Buenos Aires', 'Argentina', True), 
        ('1978-06-02', 'Argentina', 'Hungary', 2, 1, 'FIFA World Cup', 'Buenos Aires', 'Argentina', False), 
        ('1978-06-02', 'France', 'Italy', 1, 2, 'FIFA World Cup', 'Mar del Plata', 'Argentina', True), 
        ('1978-06-02', 'Mexico', 'Tunisia', 1, 3, 'FIFA World Cup', 'Rosario', 'Argentina', True), 
        ('1978-06-03', 'Austria', 'Spain', 2, 1, 'FIFA World Cup', 'Buenos Aires', 'Argentina', True), 
        ('1978-06-03', 'Brazil', 'Sweden', 1, 1, 'FIFA World Cup', 'Mar del Plata', 'Argentina', True), 
        ('1978-06-03', 'Iran', 'Netherlands', 0, 3, 'FIFA World Cup', 'Mendoza', 'Argentina', True), 
        ('1978-06-03', 'Peru', 'Scotland', 3, 1, 'FIFA World Cup', 'Córdoba', 'Argentina', True), 
        ('1978-06-06', 'Argentina', 'France', 2, 1, 'FIFA World Cup', 'Buenos Aires', 'Argentina', False), 
        ('1978-06-06', 'Germany', 'Mexico', 6, 0, 'FIFA World Cup', 'Córdoba', 'Argentina', True), 
        ('1978-06-06', 'Hungary', 'Italy', 1, 3, 'FIFA World Cup', 'Mar del Plata', 'Argentina', True), 
        ('1978-06-06', 'Poland', 'Tunisia', 1, 0, 'FIFA World Cup', 'Rosario', 'Argentina', True), 
        ('1978-06-07', 'Austria', 'Sweden', 1, 0, 'FIFA World Cup', 'Buenos Aires', 'Argentina', True), 
        ('1978-06-07', 'Brazil', 'Spain', 0, 0, 'FIFA World Cup', 'Mar del Plata', 'Argentina', True), 
        ('1978-06-07', 'Iran', 'Scotland', 1, 1, 'FIFA World Cup', 'Córdoba', 'Argentina', True), 
        ('1978-06-07', 'Netherlands', 'Peru', 0, 0, 'FIFA World Cup', 'Mendoza', 'Argentina', True), 
        ('1978-06-10', 'Argentina', 'Italy', 0, 1, 'FIFA World Cup', 'Buenos Aires', 'Argentina', False), 
        ('1978-06-10', 'France', 'Hungary', 3, 1, 'FIFA World Cup', 'Mar del Plata', 'Argentina', True), 
        ('1978-06-10', 'Germany', 'Poland', 1, 3, 'FIFA World Cup', 'Rosario', 'Argentina', True), 
        ('1978-06-11', 'Austria', 'Brazil', 0, 1, 'FIFA World Cup', 'Mar del Plata', 'Argentina', True), 
        ('1978-06-11', 'Iran', 'Peru', 1, 4, 'FIFA World Cup', 'Córdoba', 'Argentina', True), 
        ('1978-06-11', 'Netherlands', 'Scotland', 2, 3, 'FIFA World Cup', 'Mendoza', 'Argentina', True), 
        ('1978-06-11', 'Spain', 'Sweden', 1, 0, 'FIFA World Cup', 'Buenos Aires', 'Argentina', True), 
        ('1978-06-14', 'Argentina', 'Poland', 2, 0, 'FIFA World Cup', 'Rosario', 'Argentina', False), 
        ('1978-06-14', 'Austria', 'Netherlands', 1, 5, 'FIFA World Cup', 'Córdoba', 'Argentina', True), 
        ('1978-06-14', 'Brazil', 'Peru', 3, 0, 'FIFA World Cup', 'Mendoza', 'Argentina', True), 
        ('1978-06-14', 'Germany', 'Italy', 0, 0, 'FIFA World Cup', 'Buenos Aires', 'Argentina', True), 
        ('1978-06-18', 'Argentina', 'Brazil', 0, 0, 'FIFA World Cup', 'Rosario', 'Argentina', False), 
        ('1978-06-18', 'Austria', 'Italy', 0, 1, 'FIFA World Cup', 'Buenos Aires', 'Argentina', True), 
        ('1978-06-18', 'Germany', 'Netherlands', 2, 2, 'FIFA World Cup', 'Córdoba', 'Argentina', True), 
        ('1978-06-18', 'Peru', 'Poland', 0, 1, 'FIFA World Cup', 'Mendoza', 'Argentina', True), 
        ('1978-06-21', 'Argentina', 'Peru', 6, 0, 'FIFA World Cup', 'Rosario', 'Argentina', False), 
        ('1978-06-21', 'Austria', 'Germany', 3, 2, 'FIFA World Cup', 'Córdoba', 'Argentina', True), 
        ('1978-06-21', 'Brazil', 'Poland', 3, 1, 'FIFA World Cup', 'Mendoza', 'Argentina', True), 
        ('1978-06-21', 'Italy', 'Netherlands', 1, 2, 'FIFA World Cup', 'Buenos Aires', 'Argentina', True), 
        ('1978-06-24', 'Brazil', 'Italy', 2, 1, 'FIFA World Cup', 'Buenos Aires', 'Argentina', True), 
        ('1978-06-25', 'Argentina', 'Netherlands', 3, 1, 'FIFA World Cup', 'Buenos Aires', 'Argentina', False)
]

# -----------------------------------------------------------------------------------------------------
# listes des fonctions à implémenter
# -----------------------------------------------------------------------------------------------------

# Fonctions à implémenter dont les tests sont fournis


def equipe_gagnante(match):
    """retourne le nom de l'équipe qui a gagné le match. Si c'est un match nul on retourne None

    Args:
        match (tuple): un match

    Returns:
        str: le nom de l'équipe gagnante (ou None si match nul)
    """    
    if match[3] != match[4] :#si le match n'est pas nul
        if match[3] > match[4]: #si le score de l'équipe a est plus élevé
            return match[1]
        return match[2]
    return None
            

def victoire_a_domicile(match):
    """indique si le match correspond à une victoire à domicile

    Args:
        match (tuple): un match

    Returns:
        bool: True si le match ne se déroule pas en terrain neutre et que l'équipe qui reçoit a gagné
    """    
    if equipe_gagnante(match) == match[1] :
        return True
    return False


def nb_buts_marques(match):
    """indique le nombre total de buts marqués lors de ce match

    Args:
        match (tuple): un match

    Returns:
        int: le nombre de buts du match 
    """    
    return match[3] + match[4]



def matchs_ville(liste_matchs, ville):
    """retourne la liste des matchs qui se sont déroulés dans une ville donnée
    
    Args:
        liste_matchs (list): une liste de matchs
        ville (str): le nom d'une ville

    Returns:
        list: la liste des matchs qui se sont déroulé dans la ville ville    
    """
    res = []
    for (date, equipeA, equipeB, scoreA, scoreB, championnat, ville_match, pays, nul) in liste_matchs:
        if ville_match == ville:
            res.append((date, equipeA, equipeB, scoreA, scoreB, championnat, ville_match, pays, nul))
    return res
    
def nombre_moyen_buts(liste_matchs, nom_competition):
    """retourne le nombre moyen de buts marqués par match pour une compétition donnée

    Args:
        liste_matchs (list): une liste de matchs
        nom_competition (str): le nom d'une compétition
    
    Returns:
        float: le nombre moyen de buts par match pour la compétition
    """
    compteur = 0
    res = 0
    for (date, equipeA, equipeB, scoreA, scoreB, championnat, ville_match, pays, nul) in liste_matchs:
         if championnat == nom_competition:
             res += nb_buts_marques((date, equipeA, equipeB, scoreA, scoreB, championnat, ville_match, pays, nul))
             compteur += 1
    return res/compteur

def est_bien_trie(liste_matchs):
    """vérifie si une liste de matchs est bien trié dans l'ordre chronologique
       puis pour les matchs se déroulant le même jour, dans l'ordre alphabétique
       des équipes locales

    Args:
        liste_matchs (list): une liste de matchs

    Returns:
        bool: True si la liste est bien triée et False sinon
    """    
    for ind in range(1,len(liste_matchs)):
        if liste_matchs[ind][0] < liste_matchs[ind-1][0] or (liste_matchs[ind][0] == liste_matchs[ind-1][0] and liste_matchs[ind][1] < liste_matchs[ind-1][1]):
            return False
    return True 

def fusionner_matchs(liste_matchs1, liste_matchs2):
    """Fusionne deux listes de matchs triées sans doublons en une liste triée sans doublon
    sachant qu'un même match peut être présent dans les deux listes

    Args:
        liste_matchs1 (list): la première liste de matchs
        liste_matchs2 (list): la seconde liste de matchs

    Returns:
        list: la liste triée sans doublon comportant tous les matchs de liste_matchs1 et liste_matchs2
    """ 
    if est_bien_trie(liste_matchs1) and est_bien_trie(liste_matchs2):
        res = []
        ind_liste1 = 0
        ind_liste2 = 0
        while ind_liste1 < len(liste_matchs1) and ind_liste2 < len(liste_matchs2):
            if liste_matchs1[ind_liste1] == liste_matchs2[ind_liste2]:
                res.append(liste_matchs1[ind_liste1])
                ind_liste1 += 1
                ind_liste2 += 1
            if est_bien_trie([liste_matchs1[ind_liste1], liste_matchs2[ind_liste2]]):
                res.append(liste_matchs1[ind_liste1])
                ind_liste1 += 1
            else:
                res.append(liste_matchs2[ind_liste2])
                ind_liste2 += 1
        if ind_liste2 < len(liste2):
            while ind_liste2 < len(liste_matchs2):
                res.append(liste_matchs2[ind_liste2])
                ind_liste2 += 1
        if ind_liste1 < len(liste1):
            while ind_liste1 < len(liste_matchs1):
                res.append(liste_matchs1[ind_liste1])
                ind_liste1 += 1
        return res
    return None

def resultats_equipe(liste_matchs, equipe):
    """donne le nombre de victoire, de matchs nuls et de défaites pour une équipe donnée

    Args:
        liste_matchs (list): une liste de matchs
        equipe (str): le nom d'une équipe (pays)

    Returns:
        tuple: un triplet d'entiers contenant le nombre de victoires, nuls et défaites de l'équipe
    """    
    win = 0
    loose = 0
    null = 0
    for match in liste_matchs:
        if (match[1] or match[2]) == equipe:
            if equipe_gagnante(match) == equipe:
                win += 1
            elif equipe_gagnante(match) is None:
                null += 1
            else:
                loose += 1 
    return (win,null,loose)



def plus_gros_scores(liste_matchs):
    """retourne la liste des matchs pour lesquels l'écart de buts entre le vainqueur et le perdant est le plus grand

    Args:
        liste_matchs (list): une liste de matchs

    Returns:
        list: la liste des matchs avec le plus grand écart entre vainqueur et perdant
    """   
    res = [] 
    max_but = None
    for match in liste_matchs:
        if match[3] > match[4]:
            difference = match[3] - match[4]
        else:
            difference = match[4] - match[3]
        if max_but is None or difference > max_but :
            max_but = nb_buts_marques(match)
            res = [match]
        elif difference == max_but:
            res.append(match)
    return res



def liste_des_equipes(liste_matchs):
    """retourne la liste des équipes qui ont participé aux matchs de la liste
    Attention on ne veut voir apparaitre le nom de chaque équipe qu'une seule fois

    Args:
        liste_matchs (list): une liste de matchs

    Returns:
        list: une liste de str contenant le noms des équipes ayant jouer des matchs
    """
    nom_participant = []
    for (_, equipeA, equipeB, _, _, _, _, _, _) in liste_matchs:
        if equipeA not in nom_participant:
            nom_participant.append(equipeA)
        if equipeB not in nom_participant:
            nom_participant.append(equipeB)
    return nom_participant

def premiere_victoire(liste_matchs, equipe):
    """retourne la date de la première victoire de l'equipe. Si l'equipe n'a jamais gagné de match on retourne None

    Args:
        liste_matchs (list): une liste de matchs
        equipe (str): le nom d'une équipe (pays)

    Returns:
        str: la date de la première victoire de l'equipe
    """    
    if est_bien_trie(liste_matchs):
        for match in liste_matchs:
            if equipe_gagnante(match) == equipe:
                return match[0]
    return None


def nb_matchs_sans_defaites(liste_matchs, equipe):
    """retourne le plus grand nombre de matchs consécutifs sans défaite pour une equipe donnée.

    Args:
        liste_matchs (list): une liste de matchs
        equipe (str): le nom d'une équipe (pays)

    Returns:
        int: le plus grand nombre de matchs consécutifs sans défaite du pays nom_pays
    """
    consecutif = 0
    max_consecutif = 0
    for match in liste_matchs:
        if equipe_gagnante(match) == equipe:
            consecutif += 1
            if max_consecutif < consecutif:
                max_consecutif = consecutif
        else:
            consecutif = 0
    return consecutif


def charger_matchs(nom_fichier):
    """charge un fichier de matchs donné au format CSV en une liste de matchs

    Args:
        nom_fichier (str): nom du fichier CSV contenant les matchs

    Returns:
        list: la liste des matchs du fichier
    """ 
    try:  
        contenue_fic = [] 
        fic = open(nom_fichier,"r")
        fic.readline()
        for ligne in fic:
            detail_match = ligne.split(",")
            contenue_fic.append((detail_match[0] , detail_match[1] , detail_match[2] , int(detail_match[3]) , int(detail_match[4]) , detail_match[5] , detail_match[6] , detail_match[7] , detail_match[8].rstrip() == "True")) # == permet d'écrire True ou False en bool.rstrip() supprime /n
        fic.close()
        return contenue_fic
    except:
        return "fichier n'existe pas"


def sauver_matchs(liste_matchs,nom_fichier):
    """sauvegarde dans un fichier au format CSV une liste de matchs

    Args:
        liste_matchs (list): la liste des matchs à sauvegarder
        nom_fichier (str): nom du fichier CSV

    Returns:
        None: cette fonction ne retourne rien
    """    
    fic = open(nom_fichier,'w')

    for ligne in liste_matchs:
        print(ligne)
        fic.write(str(ligne) + ",")
    fic.close()


# Fonctions à implémenter dont il faut également implémenter les tests


def plus_de_victoires_que_defaites(liste_matchs, equipe):
    """vérifie si une équipe donnée a obtenu plus de victoires que de défaites
    Args:
        liste_matchs (list): une liste de matchs
        equipe (str): le nom d'une équipe (pays)

    Returns:
        bool: True si l'equipe a obtenu plus de victoires que de défaites
    """
    if len(liste_matchs) != 0:
        victoire = 0
        defaite = 0
        for match in liste_matchs:
            if equipe_gagnante(match) == equipe:
                victoire += 1
            elif match[3] == equipe or match[4] == equipe:
                defaite +=1
        if victoire != 0 or defaite != 0:  
            return victoire > defaite
    return None     

def matchs_spectaculaires(liste_matchs):
    """retourne la liste des matchs les plus spectaculaires, c'est à dire les
    matchs dont le nombre total de buts marqués est le plus grand

    Args:
        liste_matchs (list): une liste de matchs

    Returns:
        list: la liste des matchs les plus spectaculaires
    """
    but_max = None
    list_spec = []
    for match in liste_matchs:
        if but_max is None or nb_buts_marques(match) > but_max:
            but_max = nb_buts_marques(match)
            list_spec = [match]
        elif nb_buts_marques(match) == but_max:
            list_spec.append(match)
    return list_spec


def meilleures_equipes(liste_matchs):
    """retourne la liste des équipes de la liste qui ont le plus petit nombre de defaites

    Args:
        liste_matchs (list): une liste de matchs

    Returns:
        list: la liste des équipes qui ont le plus petit nombre de defaites
    """
    dic_defaite = {}
    #recherche les équipe de chaque match et les met dans un dictionnaire avec leur nombre de défaite
    for match in liste_matchs:
        if match[3] != match[4] :#si le match n'est pas nul
            if match[3] < match[4]: #si le score de l'équipe a est plus élevé
                if match[1] in dic_defaite: #si le nom de l'équipe est dans le dict on ajoute 1 à son nombre de défaite
                    dic_defaite[match[1]] += 1
                else: #si elle n'y est pas on initialise sa valeur a 1
                    dic_defaite[match[1]] = 1
                if match[2] in dic_defaite:
                    dic_defaite[match[2]] += 0
                else:
                    dic_defaite[match[2]] = 0
            else:
                if match[2] in dic_defaite:
                    dic_defaite[match[2]] += 1
                else:
                    dic_defaite[match[2]] = 1
                if match[1] in dic_defaite:
                    dic_defaite[match[1]] += 0
                else:
                    dic_defaite[match[1]] = 0

    #recherche l'équpe avec le plus petit nombre de défaite 
    min_defaite = 0
    l_defaite = []
    for cle_valeur in dic_defaite.items():
        if l_defaite == [] or cle_valeur[1] < min_defaite:
            l_defaite = [cle_valeur[0]]
            min_defaite = cle_valeur[1]
        elif cle_valeur[1] == min_defaite: #regardes si l'equipe en cours de traitement a autant de défaite que les autres
            l_defaite.append(cle_valeur[0])
    return l_defaite

  





