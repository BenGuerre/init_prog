import histoire2foot


# Ici vos fonctions dédiées aux interactions
def afficher_menu(titre, liste_options):
    """Permet d'affcher un menu

    Args:
        titre (str): Le titre du menu
        liste_options (list): Une list dEs options en str
    """
    decoration = "-" * (len(titre) + 2) #avoir un décoration aussi long que le titre
    print("      " + "+" + decoration + "+")
    print("      " + "| " +  titre + " |")
    print("      " + "+" + decoration + "+")
    for ind in range(len(liste_options)):
        print(str(ind+1) + " -> " + str(liste_options[ind]))

def demander_nombre(message, borne_max):
    """Demande un nombre

    Args:
        message (str): le message à afficher
        borne_max (int): le nombre de posssibillité

    Returns:
        int: la demande
    """
    affiche = input(message + "[" + "1" + "-" + str(borne_max) + "]"  + "\n")
    try:#regarde si affiche est un nombre
        if int(affiche) in range(1, borne_max+1):# regarde si affiche est dans la borne_max
            return int(affiche)
        else:
            demander_nombre(message, borne_max)
    except:
        demander_nombre(message, borne_max)

def menu(titre, liste_options):
    """affiche le menu

    Args:
        titre (str): le titre du menu
        liste_options (list): une liste des différentes options
    """
    afficher_menu(titre, liste_options)
    return demander_nombre("Entrez votre choix ", len(liste_options))

def nb_but_match(l_match):
    """Trouve le nombre de but par match

    Args:
        l_match (list): Une liste de match

    Returns:
        list: la liste des buts par match
    """
    l_but = []
    for ind in range(len(l_match)):
        l_but.append("Il y a eu " + str(histoire2foot.nb_buts_marques(l_match[ind])) + " but(s) demarqué au " + str(ind + 1) + "eme match ")
    return l_but

def plus_moins_victoire(l_match, equipe):
    """dit si une équipe a une plus de victoire que de défaite

    Args:
        l_match (list): une liste de matchs
        equipe (str): le nom de l'équipe

    Returns:
        str: si l'équipe a plus ou moins de victoire que de défaite
    """
    if histoire2foot.plus_de_victoires_que_defaites(l_match, equipe):
        return "plus de victoire que de défaite"
    else:
        return 'moins de victoire que de défaite'
# ici votre programme principal


def programme_principal():
    """permet utiliser et afficher menu principal
    """
    fin = False
    
    while not fin:
        try:
            l_match = histoire2foot.charger_matchs(input("Donnez le nom du fichier à traiter \n"))
            
            choix = menu("Menu Principal", ["Des informations sur un fichier", "Des informations sur une équipe", "Quitter"])
            if choix == 1:
                choix = menu("Quoi Faire ?",["Donnez les matchs se déroulant dans une ville", "donnez le nombre de but moyen d'une compétition", "Le fichier est -il bien triée", "Afficher le nombre de buts par mmchs", "Les matchs spéctaculaire", "Le moins de défaites", "Liste des équipes", "Quitter"])
                if choix == 1:
                    print(histoire2foot.matchs_ville(l_match, input("Donnez le nom de la ville \n")))
                if choix == 2:
                    print(histoire2foot.nombre_moyen_buts(l_match, input("Entrez le nom de la compétition \n")))
                if choix == 3:
                    print(histoire2foot.est_bien_trie(l_match))
                if choix == 4:
                    print(nb_but_match(l_match))
                if choix == 5:
                    print(histoire2foot.matchs_spectaculaires(l_match))
                if choix == 6:
                    print(histoire2foot.meilleures_equipes(l_match))
                if choix == 7:
                    print(histoire2foot.liste_des_equipes(l_match))
                if choix == 8:
                    fin = True
            if choix == 2:
                equipe = input("Donnez le nom de l'équipe \n")
                resultat_eq = histoire2foot.resultats_equipe(l_match, equipe)
                prem_vic = str(histoire2foot.premiere_victoire(l_match, equipe))
                print(equipe + "  a eu " + plus_moins_victoire(l_match, equipe) + ". \n" + "Elle a " + str(resultat_eq[0]) + " victoires(sa première était le " + prem_vic + "), " + str(resultat_eq[2]) + " défaites " + str(resultat_eq[1]) + " matchs nul. \n" )
            if choix == 3:
                fin = True
        except: #affiche un message en cas d'erreur et on retourne au menu principal 
            print("Erreur(Vérifiez si le fichier existe dans le dossier courant")
            programme_principal()
    print("Au revoir")

print(programme_principal())
